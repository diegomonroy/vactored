<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array(
										'servicios',
										'sondeo-de-redes-de-alcantarillado-y-complementos',
										'lavado-de-tuberia',
										'lavado-y-desinfeccion-de-areas',
										'limpieza-de-pozos-septicos-y-trampas-de-grasas',
										'inspeccion-de-camara-cctv',
										'reparaciones-puntuales-dirigida',
										'prueba-de-hermeticidad',
										'suministros-de-agua-potable',
										'certificados-de-tratamiento-y-disposicion-final'
									) ) ) : dynamic_sidebar( 'banner_servicios' ); endif; ?>
				<?php if ( is_page( array( 'contacto' ) ) ) : dynamic_sidebar( 'banner_contacto' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->